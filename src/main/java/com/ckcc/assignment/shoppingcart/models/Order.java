package com.ckcc.assignment.shoppingcart.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="tb_order")
public class Order {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="customerID")
	private Customer customer;
	
	private java.sql.Timestamp orderDate;
	private double discount;
	private double total;
	private String remark;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="orderID")
	private List<OrderDetail> orderDetails;

	public Order() {}
	public Order(Customer customer) {
		super();
		this.customer = customer;
		this.orderDate = new java.sql.Timestamp(new java.util.Date().getTime());
		this.discount = customer.getShoppingCart().getDiscount();
		this.total = customer.getShoppingCart().calculateTotal();
		this.remark = "unknown";
		
		this.orderDetails = new ArrayList<OrderDetail>();
		for(Purchase p: customer.getShoppingCart().getPurchasedItems()) {
			OrderDetail od = new OrderDetail(p.getProduct().getID(), p.getQty(), p.getDiscount(), p.getSubTotal(), p.getPrice());
			this.orderDetails.add(od);
		}
	}
	
	public void addOrderDetail(OrderDetail item) {
		orderDetails.add(item);
	}
	
}
