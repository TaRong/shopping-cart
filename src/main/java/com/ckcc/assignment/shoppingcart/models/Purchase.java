package com.ckcc.assignment.shoppingcart.models;

public class Purchase {
	
	private static int currentOrderNO;
	private String orderNo;
	private Product product;
	private double qty;
	private double discount;
	
	public Purchase(Product product, double qty, double discount) {
		super();
		this.orderNo = Integer.toString(++ Purchase.currentOrderNO);
		this.product = product;
		this.qty = qty;
		this.discount = discount;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public double getQty() {
		return qty;
	}
	
	public double getDiscount() {
		return discount;
	}
	
	public double getSubTotal() {
		return qty * product.getPrice();
	}
	
	public double getPrice() {
		return getSubTotal() * (1 - discount);
	}
	
	public Object[] getDataModel() {
		return new Object[] { orderNo, product.getName(), qty, product.getPrice(), (discount * 100) + "%", getPrice() };
	}
	
}
