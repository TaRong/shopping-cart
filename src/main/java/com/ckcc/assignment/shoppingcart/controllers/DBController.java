package com.ckcc.assignment.shoppingcart.controllers;

import java.util.List;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import com.ckcc.assignment.shoppingcart.models.*;

public class DBController {
	
	public static Cart myCart = new Cart();
	
	public static boolean addProduct(Product p) {
		
    	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
													.addAnnotatedClass(Product.class)
													.buildSessionFactory();
		Session session= factory.getCurrentSession();
		try {
			session.beginTransaction();
			session.save(p);
			session.getTransaction().commit();
			session.close();
			return true;
		}catch(Exception e){
			return false;
		}finally {
			factory.close();
		}
    }
	
	@SuppressWarnings("unchecked")
	public static List<Product> getProductList() {
		
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
													.addAnnotatedClass(Product.class)
													.buildSessionFactory();
		Session session= factory.getCurrentSession();
		List<Product> products;
		try {
			session.beginTransaction();
			products = session.createQuery("from Product").getResultList();
			session.close();
		}finally {
			factory.close();
		}
		return products;
	}

	public static Product getProduct(int pID) {
		
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
													.addAnnotatedClass(Product.class)
													.buildSessionFactory();
		Session session= factory.getCurrentSession();
		Product product;
		try {
			session.beginTransaction();
			product = session.get(Product.class, pID);
			session.close();
		}finally {
			factory.close();
		}
		return product;
	}
	
	public static boolean addOrder(Customer byCustomer) {
			
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
													.addAnnotatedClass(Customer.class)
													.addAnnotatedClass(Order.class)
													.addAnnotatedClass(OrderDetail.class)
													.buildSessionFactory();
		Session session= factory.getCurrentSession();
		try {
			Order order = new Order(byCustomer);
			session.beginTransaction();
			session.save(order);
			session.getTransaction().commit();
			session.close();
			return true;
		}finally {
			factory.close();
		}
	}
	
}
