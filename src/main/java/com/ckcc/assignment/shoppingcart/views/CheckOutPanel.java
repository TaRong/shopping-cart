package com.ckcc.assignment.shoppingcart.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import com.ckcc.assignment.shoppingcart.controllers.DBController;
import com.ckcc.assignment.shoppingcart.models.Customer;

public class CheckOutPanel extends MyPanel {
	
	private JTextField txtID;
	private JTextField txtName;
	private JTextField txtEmail;
	private JTextField txtShippingAddress;
	private JTextField txtBillingAddress;
	private JTextField txtDiscount;
	private JTextField txtPhone;
	private JPanel panel;
	private CheckOutDetailPanel panel1;

	/**
	 * Create the panel.
	 */
	public CheckOutPanel(final MainForm parent) {
		setLayout(new GridLayout(0, 1, 0, 0));
		
		panel = new JPanel();
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panelTitle1 = new JPanel();
		panel.add(panelTitle1, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Before Check out, Give me your information :");
		panelTitle1.add(lblNewLabel);
		
		JPanel panelView1 = new JPanel();
		panel.add(panelView1, BorderLayout.CENTER);
		panelView1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblEnterYourIdentification = new JLabel("Enter your identification number :");
		panelView1.add(lblEnterYourIdentification);
		
		txtID = new JTextField();
		txtID.setText("110022");
		panelView1.add(txtID);
		txtID.setColumns(10);
		
		JLabel lblEnterYourName = new JLabel("Enter your name :");
		panelView1.add(lblEnterYourName);
		
		txtName = new JTextField();
		txtName.setText("Mr A");
		panelView1.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblEnterYourEmail = new JLabel("Enter your email :");
		panelView1.add(lblEnterYourEmail);
		
		txtEmail = new JTextField();
		txtEmail.setText("a@gmail.com");
		panelView1.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Enter your phone number :");
		panelView1.add(lblNewLabel_3);
		
		txtPhone = new JTextField();
		txtPhone.setText("012 123 234");
		panelView1.add(txtPhone);
		txtPhone.setColumns(10);
		
		JLabel lblEnterYourShipping = new JLabel("Enter your shipping address :");
		panelView1.add(lblEnterYourShipping);
		
		txtShippingAddress = new JTextField();
		txtShippingAddress.setText("this place");
		panelView1.add(txtShippingAddress);
		txtShippingAddress.setColumns(10);
		
		JLabel lblEnterYourBilling = new JLabel("Enter your billing address :");
		panelView1.add(lblEnterYourBilling);
		
		txtBillingAddress = new JTextField();
		txtBillingAddress.setText("here it is");
		panelView1.add(txtBillingAddress);
		txtBillingAddress.setColumns(10);
		
		JLabel lblEnterPercentageOn = new JLabel("Enter percentage on your discount card (%) :");
		panelView1.add(lblEnterPercentageOn);
		
		txtDiscount = new JTextField();
		txtDiscount.setText("10");
		panelView1.add(txtDiscount);
		txtDiscount.setColumns(10);
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnEnterEvent();
			}
		});
		panelView1.add(btnEnter);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.returnView();
			}
		});
		panelView1.add(btnClose);

	}

	private void btnEnterEvent() {
		String title = "", message = ""; 
		try {
			//get data
			int id = Integer.parseInt(txtID.getText());
			String name = txtName.getText();
			String email = txtEmail.getText();
			String phone = txtPhone.getText();
			String shippingAddress = txtShippingAddress.getText();
			String billingAddress = txtBillingAddress.getText();
			double cardDiscount = Double.parseDouble(txtDiscount.getText()) / 100;
			// Set discount on card
			DBController.myCart.setDiscount(cardDiscount);
			// Create customer object
			Customer newCustomer = new Customer(id, name, email, phone, shippingAddress, billingAddress);
			// Place order to customer
			newCustomer.placeOrder(DBController.myCart);
			//add order to database
			boolean isCompleted = DBController.addOrder(newCustomer);
			if(isCompleted) {
				title = "Checked Out.";
				message = "Customer " + name + " has been completed order and inserted to database.";
				//change input view to details view
				panel1 = new CheckOutDetailPanel();
				panel1.showData(newCustomer);
				//clear all purchased items
				DBController.myCart.clearPurchasedItem();
			}else {
				title = "Failed.";
				message = "Cannot insert to database.";
				return;
			}
		
			refreshView();
			
		}catch (NumberFormatException e) {
			// TODO: handle exception
			title = "Incorrect Input.";
			message = "Please input number-only at identification number and discount card.";
		}finally {
			JOptionPane.showMessageDialog(this, message, title, JOptionPane.PLAIN_MESSAGE);
		}
		
	}
	
	private void refreshView(){
		panel.removeAll();
		panel.add(panel1);
		revalidate();
		repaint();
	}

	public void init() {
		// TODO Auto-generated method stub
		txtID.requestFocus();
	}
}
