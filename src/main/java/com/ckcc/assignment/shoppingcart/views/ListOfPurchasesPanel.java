package com.ckcc.assignment.shoppingcart.views;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.ckcc.assignment.shoppingcart.controllers.DBController;
import com.ckcc.assignment.shoppingcart.models.Purchase;

public class ListOfPurchasesPanel extends JPanel {
	private JTable table;

	/**
	 * Create the panel.
	 */
	public ListOfPurchasesPanel() {
		setLayout(new BorderLayout(0, 0));

		table = new JTable();
		table.setRowSelectionAllowed(false);
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {"ID", "Name", "Quantity", "Unit Price", "Discount", "Price"}));
		table.setFocusable(false);
		add(table.getTableHeader(), BorderLayout.NORTH);
		add(table, BorderLayout.CENTER);
		
		insertItemToTable();
		
	}

	private void insertItemToTable() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
		for(Purchase p: DBController.myCart.getPurchasedItems()) {
			model.addRow(p.getDataModel());
		}
	}
	
}
