package com.ckcc.assignment.shoppingcart.views;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public abstract class MyPanel extends JPanel {
	//add listener to all needed components
	public void addListionerForComponents(JPanel panel) {
		//get components
		for(Component c: panel.getComponents()) {
			//for text field
			if(c instanceof JTextField) {
				final JTextField tf = ((JTextField) c);
				tf.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
					}
				});
				tf.addFocusListener(new FocusListener() {
					public void focusGained(FocusEvent arg0) {
						// TODO Auto-generated method stub
						tf.selectAll();
					}
					public void focusLost(FocusEvent arg0) {}
				});
			//for button
			}else if(c instanceof JButton) {
				final JButton btn = (JButton) c;
				btn.addKeyListener(new KeyListener() {
					public void keyTyped(KeyEvent arg0) {
						// TODO Auto-generated method stub
						btn.doClick();
					}
					public void keyReleased(KeyEvent arg0) {}
					public void keyPressed(KeyEvent arg0) {}
				});
			//for panel by recursive
			}else if(c instanceof JPanel){
				addListionerForComponents((JPanel) c);
			}
		}
	}
	//method for startup action
	public abstract void init();
	
}
