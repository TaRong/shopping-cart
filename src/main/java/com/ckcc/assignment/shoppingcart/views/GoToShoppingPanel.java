package com.ckcc.assignment.shoppingcart.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import com.ckcc.assignment.shoppingcart.controllers.DBController;
import com.ckcc.assignment.shoppingcart.models.*;

public class GoToShoppingPanel extends MyPanel {
	
	private JTextField txtProductID;
	private JTextField txtProductQty;
	private JTextField txtProductDiscount;

	/**
	 * Create the panel.
	 */
	public GoToShoppingPanel(final MainForm parent) {
		
		setLayout(new GridLayout(0, 2, 5, 5));
		
		JLabel lblProductsCodeYou = new JLabel("Product's id you want to buy :");
		add(lblProductsCodeYou);
		
		txtProductID = new JTextField();
		add(txtProductID);
		txtProductID.setColumns(10);
		
		JLabel lblProductsQtyYou = new JLabel("Product's qty you want to buy :");
		add(lblProductsQtyYou);
		
		txtProductQty = new JTextField();
		add(txtProductQty);
		txtProductQty.setColumns(10);
		
		JLabel lblPercentageDiscountFor = new JLabel("Percentage discount for this product if you have ?(%)");
		add(lblPercentageDiscountFor);
		
		txtProductDiscount = new JTextField();
		add(txtProductDiscount);
		txtProductDiscount.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSaveEvent();
			}
		});
		add(btnSave);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.returnView();
			}
		});
		add(btnClose);

	}
	
	private void btnSaveEvent() {
		String title = "", message = "";
		try {
			int id = Integer.parseInt(txtProductID.getText());
			Product p = DBController.getProduct(id);
			// Check product code is exist
			if(p == null) {
				title = "Product Not Found";
				message = "Product with id " + id + " is not found in database to purchase.";
				return;
			}
			double qty = Double.parseDouble(txtProductQty.getText());
			if(qty <= 0) qty = 1;
			// Check stock is valid
			if(p.isValidStock(qty) == false) {
				title = "Not Enough Stock";
				message = "Not enough stock!! product with id " + id + " has only " + p.getQtyInStock() + " in stock.";
				return;
			}
			double discount = Double.parseDouble(txtProductDiscount.getText()) / 100;
			if(discount < 0 || discount >= 1) discount = 0;
			// add object of purchase item to controller
			DBController.myCart.addItem(new Purchase( p, qty, discount));
			title = "Order Complete";
			message = "Product with id " + id + " has been ordered with " + qty + " quantities.";
		}catch(NumberFormatException e) {
			title = "Incorrected Input.";
			message = "Please input number only at any fields.";
		}finally {
			JOptionPane.showMessageDialog(this, message, title, JOptionPane.PLAIN_MESSAGE);
		}
		
	}

	public void init() {
		// TODO Auto-generated method stub
		txtProductID.requestFocus();
	}

}
